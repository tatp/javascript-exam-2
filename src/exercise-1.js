const getCommonItems = (array1, array2) => {
  // implement code here
  return array1.filter(value => array2.includes(value))
};

export default getCommonItems;
