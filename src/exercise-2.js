const getTheExactNumber = (numbers) => {
  // implement code here
  return Math.max(...numbers.filter(value => value % 3 == 0))
}

export default getTheExactNumber;